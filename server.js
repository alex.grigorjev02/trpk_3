const express = require('express');
const path = require('path');
const app = express()
const port = 1430

app.use(express.static(path.join(__dirname, 'public')))

app.get('/', async (req, res) => {
  res.sendFile(`${__dirname}/public/index.html`);
})


app.listen(port, () => {
  console.log(`Example app listening on port ${port}`)
})